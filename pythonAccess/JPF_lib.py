from math import *
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
import sys

sys.path[:0]=['.','/jet/share/lib/python']

from getdat import *
# PPF routines match C/fortran api 
from ppf import *


ppfsetdevice("JET")
ppfuid('JETPPF','r')

user=ppfgid('r')
user

ier=ppfgo() # if not equal to 0 is error
ier

ier=ppfgo(57872,0)
ier

ihdat,iwdat,data,x,t,ier=ppfget(57872,"S3AD","AD36")
plt.plot(t,data,'-.')
plt.axis([t.min()+6,t.max()-6,data.min(), data.max()])
plt.show()

nx,nt,format,units,comm,systat,ustat,ier=ppfdti(57872,0,"s3ad","ad36")

nseq,lseq,ldda,ndda,cdda,ier=pdlppf(57872,-1,1,256,16384)

ndt,dtnams,lxtv,dtcomms,ier=pdtinf(57872,"s3ad",-16384)

# This is very slow process to get all that info!!!!
n,dda,dtyp,seq,ier=ppfdds(57872,1,16384)


#a=[for i in dda if dda[i]=='S3AD']


# JPF data
def getJPF(nodeXXX,pulseYYY):
    """ return data in a dictionary ZZZ"""
    """ may be used to save in a MAT-file """
    import numpy as np
    import sys
    sys.path[:0]=['.','/jet/share/lib/python']
    import getdat as G
    nwds,ier=G.getnwds(nodeXXX,pulseYYY)
    if ier==0:
        data,tvec,nwds,title,units,ier=G.getdat(nodeXXX,pulseYYY,nwds)
    else:
        print 'ERROR in getnwds, ier=%d' %ier
    Z=np.hstack((tvec,data))
    ZZ=Z.reshape(tvec.size,2,order='F')
    ZZZ={'data_'+str(pulseYYY)+'_'+nodeXXX:ZZ}
    return ZZZ
        
    
#*******************************
# functions used in the script

def shotRead(XXX):
""" return a list of integer values in XXX """
    YYY=XXX.read().split()
    OUT=[]
    for i in range(0,len(YYY)):
        OUT.append(int(YYY[i]))
    return OUT
#****************************************************
def demo()
# reading the shot number into a list
    F=open('shots.dat','r'); 
    pulse=shotRead(F)
# declare node names to read from DRIVE
    node=['PF/V5-VLD2<ADC','PF/V5-VLD3<ADC']
# get JPF data
    for l in range(0,len(pulse)):
    DICOUT={}
        for m in range(0,len(node)):
                DICOUT.update(getJPF(node[m],pulse[l]))
        sio.savemat(str(pulse[l])+'.mat',DICOUT)

mat=sio.loadmat('84458.mat')
key=mat.keys()
VLD2=mat[key[0]]
VLD3=mat[key[2]]
tVLD2=VLD2[:,0]
aVLD2=VLD2[:,1]
plt.plot(tVLD2,aVLD2,'-r')
plt.show()
#
mat=sio.loadmat('84461.mat')
GAS=mat['data_84461_MAJR']
time=GAS[:,0]
signal=GAS[:,1]
plt.plot(time,signal,'-r')
plt.show()
#
tVLD2=VLD2[:,0]
aVLD2=VLD2[:,1]
tVLD3=VLD3[:,0]
aVLD3=VLD3[:,1]
plt.plot(tVLD3,aVLD3,'-r')
plt.show()


