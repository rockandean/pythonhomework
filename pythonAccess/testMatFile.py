#!/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio

mat=sio.loadmat('84461.mat')
GAS=mat['data_84461_MAJR']
time=GAS[:,0]
signal=GAS[:,1]
plt.plot(time,signal,'-r')
plt.xlabel('time s')
plt.ylabel('GAS (number of particles)')
plt.title(' Gas puffed in JET plasma 84461')
plt.show()
