#!/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
import sys
sys.path[:0]=['.','/jet/share/lib/python']
# PPF and JPF routines match C/fortran api 
from ppf import *
import getdat as G

# PPF data
def getPPF(DDA,Dtype,shot):
    """ return data in a dictionary LLL"""
    """ may be used to save in a MAT-file """
    ier=ppfgo(shot,0)
    if ier==0:
        ihdat,iwdat,dataPPF,x,tPPF,ier=ppfget(shot,DDA,Dtype)
        print 'check ppfget() error; ier = %d good!. ' % ier
    else:
        print "ERROR in ppfgo, shot can't read, ier=%d" %ier    
    L=np.hstack((tPPF,dataPPF))
    LL=L.reshape(tPPF.size,2,order='F')
    LLL={'data_'+str(shot)+'_'+Dtype:LL}
    return LLL
# JPF data
def getJPF(nodeXXX,pulseYYY):
    """ return data in a dictionary ZZZ"""
    """ may be used to save in a MAT-file """
    nwds,ier=G.getnwds(nodeXXX,pulseYYY)
    if ier==0:
        data,tvec,nwds,title,units,ier=G.getdat(nodeXXX,pulseYYY,nwds)
        print 'check getdat() error; ier = %d good!. ' % ier
    else:
        print 'ERROR in getnwds, ier=%d' %ier
    Z=np.hstack((tvec,data))
    ZZ=Z.reshape(tvec.size,2,order='F')
    ZZZ={'data_'+str(pulseYYY)+'_'+nodeXXX:ZZ}
    return ZZZ

def shotRead(XXX):
    """ return a list of integer values in XXX """
    YYY=XXX.read().split()
    OUT=[]
    for i in range(0,len(YYY)):
        OUT.append(int(YYY[i]))
    return OUT

def getdata():
    """START reading the shot numbers from shots.dat into a list"""
    """use var's: node, Dtype for required JPF and PPF data names"""
    F=open('shots.dat','r') 
    pulse=shotRead(F)
    """declare node names to read from the JET JPF database """
    node=['PF/V5-VLD2<ADC','PF/V5-VLD3<ADC']

    for l in range(0,len(pulse)):
        DICOUT={}
        for m in range(0,len(node)):
                DICOUT.update(getJPF(node[m],pulse[l]))#get JPF data
        sio.savemat(str(pulse[l])+'.mat',DICOUT)
    # end of JPF
    """declare DDA and Dtype names to read from the JET PPF database """
    Dtype=(('GASM','MAJR'),('EDG8','TBEO'),('EDG8','TBEI'))
    ppfsetdevice("JET")
    ppfuid('JETPPF','r')
    ierr=ppfgo() # if not equal to 0 is error

    if ierr==0:
        for l in range(0,len(pulse)):
            mat=sio.loadmat(str(pulse[l])+'.mat')
            PPFOUT={}
            for m in range(0,len(Dtype)):             #get PPF data
                PPFOUT.update(getPPF(Dtype[m][0],Dtype[m][1],pulse[l]))
            mat.update(PPFOUT)        
            sio.savemat(str(pulse[l])+'.mat',mat)
    else:
        print 'ERROR in ppfgo(), ier=%d' %ier

    return
    
getdata()
