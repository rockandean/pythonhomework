                PROGRAM readJPF
C**********************************************************************
C* Sergei Gerasimov 19-Sep-2003
C* This is an example how to read JPF, where
C* ISHOT - JET pulse number
C* MAXPT - max. number of time points
C* See also:  http://users.jet.efda.org/pages/
C*   codes-data/data-and-codes/activities/codes/GETDAT/index.htm
C***********INITIALISE PARAMETER SETTINGS******************************

       PARAMETER     (MAXPT =20000) 

C***********GLOBAL VARIABLES*******************************
C****** pulse number and number of time points:
       INTEGER        ISHOT, npnts
       
C******* calibrated TF uncompensated Signals, JPF names:
       CHARACTER      SignalName*19
       
C****** Even and Odd TF compensation coefficients, JPF names: *********
       CHARACTER      CoefName_E*19,CoefName_O*19

C****** Even and Odd TF currents, JPF names:         
       CHARACTER      TFEVN*19,TFODD*19

C******* Unimportant  variables:    
       CHARACTER      TITLE*30,units*10
       INTEGER        i,IERR1
       
C******* Calibrated, but TF uncompensated Coil Signal, Y and time: ****
       REAL           YDATA(MAXPT),TVECT(MAXPT)

C******* TF compensation coefficients:               
       REAL           TFC_O, TFC_E         

C******* TF currents:         
       REAL           TFEVNdata(MAXPT), TFODDdata(MAXPT)
       
C******* Result/output:       
       REAL           CoilGood(MAXPT)


C***********INPUT PARAMETERS *********

C******* calibrated TF uncompensated Signal Name:
       SignalName ='DA/C2-CX01'
       
C****** Even TF compensation coefficient:       
       CoefName_E ='DA/C2-CX01<TFE'
       
C****** Odd TF compensation coefficient:       
       CoefName_O ='DA/C2-CX01<TFO'
       
C****** Even TF current:       
       TFEVN ='DA/C2-ITFEVN'
       
C****** Odd TF current:       
       TFODD ='DA/C2-ITFODD'       
C****** ISHOT number:                 
       ISHOT = 57317
             
C*************       
       NPNTS = maxpt
       
C************* READ SIGNAL DATA  ******************************
         call GetSig(SignalName,ISHOT,ydata,tvect,npnts,title,units)
         call GetCoef(CoefName_E,ISHOT,TFC_E)
         call GetCoef(CoefName_O,ISHOT,TFC_O) 
         NPNTS = maxpt
         call GetSig(TFEVN,ISHOT,TFEVNdata,tvect,npnts,title,units)
           NPNTS = maxpt
         call GetSig(TFODD,ISHOT,TFODDdata,tvect,npnts,title,units)

C************* Calculate Result/output: TF compensated signal *********

         DO 101 I =1,NPNTS
          CoilGood(i)= ydata(i)-TFC_E*TFEVNdata(i)-TFC_O*TFODDdata(i)
  101    CONTINUE 	   

C************* WRITE TEST OUTPUT FILE 10 ******************************

          WRITE(10,*) 'Shot=',ISHOT, ',   ','Signal Name=', CoefName_E 
           WRITE(10,*) 'TF Coefficient=', TFC_E, TFC_O
 
C************* WRITE TEST OUTPUT FILE 11*******************************

         WRITE(11,*) '# Shot=',ISHOT, ',   ','Signal Name=', SignalName
         WRITE(11,*) '#', TITLE, UNITs
         WRITE(11,*) '#   Time(s)', '   Raw Signal(SI)', '  Result(SI)'
         DO 100 I =1,NPNTS
          WRITE(11,*) TVECT(I),YDATA(I), CoilGood(I), I
  100    CONTINUE 	   

        STOP
        END
c**********************************************************************
c***GetSig reads calibrated TF uncompensated Coil Signal **********
      subroutine GetSig(SignalName,ISHOT,ydata,time,ntime,title,units)
      implicit none
      integer ISHOT, ntime
      real*4 ydata(*),time(*)
      character*(*) SignalName,title,units
      integer ierr
      ierr=0
      call getdat(SignalName,ISHOT,ydata,time,ntime,title,units,ierr)
      if (ierr.ne.0) then
         write(*,*) ISHOT,' getdat error ',SignalName,' ',ierr
      end if

      return
      end
c**********************************************************************
c**** GetCoef reads TF compensation coefficient ***********************
      subroutine GetCoef(CoefName,ISHOT,TFC)
      implicit none
      integer ISHOT
      INTEGER*2      iDATA(3)      
      real*4 TFC(*)
      character*(*) CoefName,title*30
      integer ierr

      ierr=0
      CALL GETraw(CoefName,ISHOT,iDATA,3,TITLE,IERR)
      if (ierr.ne.0) then
         write(*,*) ISHOT,' getdat error ',CoefName,' ',ierr
      end if
C***** CONVERT FROM NORD 48 BIT FLOATING POINT TO 32 BIT ARRAY ********

       CALL NORD_TO_FLOAT(iDATA,TFC)

      return
      end


