      PROGRAM readJPF
      implicit none
      INTEGER       ISHOT,MAXPT,nwds
      PARAMETER     (MAXPT=1000000)
c*** JPF names:
      CHARACTER      SignalName*19
c*** Signal, Y and time: 
      REAL          YDATA(MAXPT),TVECT(MAXPT)
c*** Unimportant  variables:    
      CHARACTER      TITLE*30,units*10
      INTEGER        i,IERR1
C***********INPUT PARAMETERS *********
      SignalName ='DA/C2-VLD2'
      ISHOT=83775
      nwds=MAXPT
      print*,'calling getnwds..' 
C************* READ JPF  ******************************
      call getnwds(SignalName,ISHOT,nwds,IERR1)
      if(IERR1.ne.0)then
      write(*,*) ISHOT, ' GetNwds error, ', SignalName, ', error',IERR1
      end if
      print*,'got nwds.. total points '  ,nwds
      call GetSig(SignalName,ISHOT,ydata,tvect,nwds,title,units)

C************* WRITE TEST OUTPUT FILE 11*******************************
      print*, 'got the data'
      WRITE(6,*) '# Shot=',ISHOT, ',   ','Signal Name=', SignalName
      WRITE(6,*) '#',TITLE, UNITs
      WRITE(6,*) '#   Time(s)', '   Raw Signal(SI)'
      DO 100 I =1,3
      WRITE(6,*) TVECT(I),YDATA(I), I
 100  end do
      STOP
      END PROGRAM readJPF
c**********************************************************************
c***GetSig reads Signal **********
      subroutine GetSig(SignalName,ISHOT,ydata,time,ntime,title,units)
      implicit none
      integer ISHOT, ntime
      real*4 ydata(*),time(*)
      character*(*) SignalName,title,units
      integer ierr
      ierr=0
      print*, 'getting data...'
      
      call getdat(SignalName,ISHOT,ydata,time,ntime,title,units,ierr)
      if (ierr.ne.0) then
         write(*,*) ISHOT,' getdat error ',SignalName,' ',ierr
      end if

      return
      end
c**********************************************************************
