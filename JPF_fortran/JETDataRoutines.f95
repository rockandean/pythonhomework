PROGRAM JETDataRoutines
!!> readmodule contains:
!> FC_PPFUID(), passes the login to PPF databases with read 'r' flag. 
!> FC_DDAINF(), save DDA datatypes name-info into the file: datatype.dat.
!> 
!> The action is defined read or write, which means check on screen or save to a file.
!> the save files have the shot number and the datatype on it. 
use JETDataModules

    IMPLICIT NONE
    INTEGER,PARAMETER   ::  iwp=selected_int_kind(6,35)
    INTEGER,PARAMETER   ::  wp=selected_real_kind(12,70)
    LOGICAL,PARAMETER   ::  DEBUG=.TRUE. 
    !> DEBUG controls DDA input and some screen outputs

    INTEGER(KIND=iwp)   :: l
    INTEGER(KIND=iwp),dimension(:),allocatable   :: shots
    CHARACTER(len=13) :: shot_file_name='JET_shots.txt'
    INTEGER           :: shot_file_unit=999
    
    CHARACTER(len=3)  :: input
    
    WRITE(*,*)' Input number of shots in file "JET_shots.txt":'
    READ(*,*)l
    allocate(shots(l))
    OPEN(shot_file_unit,file=shot_file_name)
    READ(shot_file_unit,*)shot
    IF(DEBUG) WRITE(*,*)shot
    
    CLOSE(shot_file_unit)
    
    WRITE(*,*)' Insert database; PPF, JPF'
    READ(*,*)input
    
    SELECT CASE(input)
    CASE('JPF')
    WRITE(*,*)'WORKING ON IT...'
!    WRITE(*,*)' This routine is used to saved JPF data into files'
!    WRITE(*,*)' Insert JPF names datatypes'
!    WRITE(*,*)' e.g: DA/C2-VLD2 for JPF loop current'
!    READ(*,*) JPF_datatype
   
    CASE('PPF')
ppf:    DO
        WRITE(*,*)' This routine is used to saved PPF data into files'
        WRITE(*,*)' Insert DDA name'
        WRITE(*,*)' e.g: S3AD for D_alpha, GASM, EDG8, etc.' 
        !'MAGN' 'EDG8' 'MNJ3'  ! IMK2  divertor coil,'MAGF','GASM'  !MAJR
        IF(DEBUG)THEN
        idda='S3AD'
        ELSE
        READ(*,*)idda
        END IF
      CALL FC_PPFUID() !!> Passes the DDA source and "r" ,read instruction.
      CALL FC_DDAINF() !!> save datatype info into a file: datatype.dat.
      CALL FC_SAVEDATA()
      
    END DO ppf
    deallocate(shots)
END PROGRAM JETDataRoutines
